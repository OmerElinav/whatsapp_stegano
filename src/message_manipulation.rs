use std::fs::File;
use std::io::Read;
use compress::lz4::encode_block;
use compress::lz4::decode_block;

fn read_message() -> Vec<u8> {
    let mut file = File::open("res/message.txt").expect("Failed to open message.txt");
    let mut result = Vec::new();
    file.read_to_end(&mut result).expect("failed to read message.txt");
    result
}

pub fn compress() -> Vec<u8> {
    let input = read_message();
    let mut result = Vec::new();
    encode_block(&input, &mut result);
    result
}

pub fn decompress(compressed: Vec<u8>) -> String {
    let mut result = Vec::new();
    decode_block(&compressed, &mut result);
    String::from_utf8(result).expect("couldn't decompress message")
}